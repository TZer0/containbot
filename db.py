from pony.orm import *

db = Database()

class MemberRole(db.Entity):
	id = PrimaryKey(int, size=64)
	guild_id = Required(int, size=64)

class RehabRole(db.Entity):
	id = PrimaryKey(int, size=64)
	guild_id = Required(int, size=64)

class MonitoredMember(db.Entity):
	id = PrimaryKey(int, size=64)
	guild_id = Required(int, size=64)

class AlertChannel(db.Entity):
	guild_id = PrimaryKey(int, size=64)
	channel_id = Required(int, size=64)

def setupDb(name):
	set_sql_debug(False)
	db.bind(provider='sqlite', filename=name, create_db=True)
	db.generate_mapping(create_tables=True)
	commit()
