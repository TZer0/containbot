import unittest
import sys
from datetime import datetime

from disco.client import ClientConfig, Client
from disco.bot.command import Command
from disco.bot.bot import Bot
from disco.types.guild import AuditLogActionTypes

sys.path.append('..')

from pony.orm import *

import containbot
import db

class MockBot(Bot):
    @property
    def commands(self):
        return getattr(self, '_commands', [])

class Object(object):
	pass

class Guild:
	def __init__(self, id, name, members, roles, channels):
		self.id = id
		self.name = name
		self.members = members
		self.roles = roles
		self.channels = channels
		self.audit = []
	def get_audit_log_entries(self):
		return self.audit
	def setAudit(self, audit):
		self.audit = audit

class Channel:
	def __init__(self, id, name):
		self.id = id
		self.name = name
		self.msg = None
		self.embed = None
	def send_message(self, msg=None, embed=None):
		self.msg = msg
		self.embed = embed

class Member:
	def __init__(self, id, name, roles = []):
		self.id = id
		self.name = name
		self.roles = roles
		self.removed_roles = []
		self.added_roles = []
	def __str__(self):
		return self.name
	def get_scan_variants(self):
		return [str(self.id), self.name]
	def setGuild(self, guild):
		self.guild = guild
		self.guild_id = guild.id
	def remove_role(self, id):
		self.removed_roles.append(id)
	def add_role(self, id):
		self.added_roles.append(id)

class Event(object):
	def __init__(self, author, channel, guild):
		self.channel = channel
		self.rep = None
		self.embed = None
		self.guild = guild
		self.msg = Object
		self.msg.reply = self.reply
		self.member = self.author = author
	def reply(self, rep=None, embed=None):
		self.rep = rep
		self.embed = embed

class MessageEvent(Event):
	def __init__(self, author, channel, guild, content):
		super().__init__(author, channel, guild)
		self.content = content
		self.deleteCalled = False
	
	def delete(self):
		self.deleteCalled = True

class GuildEvent(object):
	def __init__(self, member, guild):
		self.member = member
		self.guild = guild
        
class Role:
	def __init__(self, id, name):
		self.id = id
		self.name = name

def getIdDict(l):
	ret = {}
	for el in l:
		ret[el.id] = el
	return ret

class RehabBotTests(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		db.setupDb("test.sqlite")
	def setUp(self):
		self.config = ClientConfig(
			{'config': 'TEST_TOKEN', 'configured_db': True}
		)
		self.client = Client(self.config)
		self.bot = MockBot(self.client)
		self.plugin = containbot.ContainBot(self.bot, self.config)
		self.setupStructure()
	
	def tearDown(self):
		db.db.drop_all_tables(with_all_data=True)
		db.db.create_tables()
		
	@db_session	
	def setupStructure(self):
		self.rehabRole = Role(4, "rehabrole")
		self.member = Role(2, "member test2")
		self.admin = Role(1, "admin test1")
		role_dict = getIdDict([self.admin, self.member, self.rehabRole])
		self.member1 = Member(600, "m1", roles=[self.admin.id])
		self.member2 = Member(700, "m2", roles=[self.member.id])

		self.good_channel = Channel(13, "rehab")
		self.another_channel = Channel(14, "rehab2")
		self.good_guild = Guild(6, "g6", getIdDict([self.member1, self.member2]), role_dict, getIdDict([self.another_channel, self.good_channel]))
		self.bad_guild = Guild(50, "g50", getIdDict([self.member1]), role_dict, getIdDict([self.another_channel, self.good_channel]))
		
		self.member1.setGuild(self.good_guild)
		self.member2.setGuild(self.good_guild)
		
		self.plugin.state.guilds = {self.good_guild.id : self.good_guild, self.bad_guild.id : self.bad_guild}
		self.plugin.client.config.admins = [self.member1.id]
	
	def getGoodEvent(self):
		return Event(self.member1, self.good_channel, self.good_guild)
	
	def getGoodEventOtherChannel(self):
		return Event(self.member1, self.another_channel, self.good_guild)
	
	def getBadUserEvent(self):
		return Event(self.member2, self.good_channel, self.good_guild)
	
	def getOkayMesssage(self):
		return MessageEvent(self.member2, self.good_channel, self.good_guild, "Hello, good day!")
		
	def getContainMesssage(self):
		return MessageEvent(self.member2, self.good_channel, self.good_guild, "kill")
	
	def getNonContainMessageMember2(self):
		return MessageEvent(self.member2, self.good_channel, self.good_guild, "hello, good day!")

	def getNonContainMessageMember1(self):
		return MessageEvent(self.member2, self.good_channel, self.good_guild, "kill")
	
	def getTestSet(self):
		return [{"addRole": self.plugin.addMemberRole, "collection": db.MemberRole, "removeRole": self.plugin.removeMemberRole},
				{"addRole": self.plugin.addMember, "collection": db.MonitoredMember, "removeRole": self.plugin.removeMember},
				{"addRole": self.plugin.addRehabRole, "collection": db.RehabRole, "removeRole": self.plugin.removeRehabRole}
		]
	
	
	@db_session
	def test_add_role(self):
		for s in self.getTestSet():
			s["addRole"](self.getGoodEvent(), 1)
			res = self.plugin.getObjectOrNone(s["collection"], 1, self.good_guild.id)
		
			self.assertEqual(res.id, 1)
			self.assertEqual(res.guild_id, self.good_guild.id)
		
	@db_session
	def test_add_role_block(self):
		for s in self.getTestSet():
			s["addRole"](self.getBadUserEvent(), 1)
			res = self.plugin.getObjectOrNone(s["collection"], 1, self.good_guild.id)
		
			self.assertIsNone(res)
		
	@db_session
	def test_del_role(self):
		for s in self.getTestSet():
			s["addRole"](self.getGoodEvent(), 1)
			res = self.plugin.getObjectOrNone(s["collection"], 1, self.good_guild.id)
		
			self.assertEqual(res.id, 1)
			self.assertEqual(res.guild_id, self.good_guild.id)
		
			s["removeRole"](self.getGoodEvent(), 1)
		
			res = self.plugin.getObjectOrNone(s["collection"], 1, self.good_guild.id)
		
			self.assertIsNone(res)
		
	@db_session
	def test_del_role_block(self):
		for s in self.getTestSet():
			s["addRole"](self.getGoodEvent(), 1)
			s["removeRole"](self.getBadUserEvent(), 1)
			res = self.plugin.getObjectOrNone(s["collection"], 1, self.good_guild.id)
		
			self.assertEqual(res.id, 1)
			self.assertEqual(res.guild_id, self.good_guild.id)
	
	@db_session
	def test_role_removal(self):
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		self.plugin.addToCollection(db.MonitoredMember, self.member2.id, self.getGoodEvent())
		
		msg = self.getContainMesssage()
		self.plugin.messageHandler(msg)
		
		self.assertTrue(msg.deleteCalled)
		self.assertEqual(len(self.member2.removed_roles), 1)
		self.assertEqual(self.member2.removed_roles[0], self.member.id)

	@db_session
	def test_role_no_removal(self):
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		msgs = (self.getOkayMesssage(), self.getNonContainMessageMember1(), self.getNonContainMessageMember2())
		for msg in msgs:
			self.plugin.messageHandler(msg)
			self.assertFalse(msg.deleteCalled)
		
		self.assertEqual(len(self.member1.removed_roles), 0)
	
	@db_session
	def test_add_rehab_role(self):
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		self.plugin.addToCollection(db.RehabRole, self.rehabRole.id, self.getGoodEvent())
		self.plugin.addToCollection(db.MonitoredMember, self.member2.id, self.getGoodEvent())

		msg = self.getContainMesssage()
		self.plugin.messageHandler(msg)
		
		self.assertTrue(msg.deleteCalled)
		
		self.assertEqual(len(self.member2.removed_roles), 1)
		self.assertEqual(len(self.member2.added_roles), 1)
		self.assertEqual(self.member2.removed_roles[0], self.member.id)
		self.assertEqual(self.member2.added_roles[0], self.rehabRole.id)
	
	@db_session
	def test_set_alert_channel_change(self):
		self.plugin.setAlertChannel(self.getGoodEvent())
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		self.plugin.addToCollection(db.MonitoredMember, self.member2.id, self.getGoodEvent())

		msg = self.getContainMesssage()
		self.plugin.messageHandler(msg)
		
		self.assertTrue(msg.deleteCalled)
		
		self.assertEqual(len(self.member2.removed_roles), 1)
		self.assertEqual(self.member2.removed_roles[0], self.member.id)
		
		self.assertTrue("m2" in self.good_channel.msg)
		self.assertTrue("rehab" in self.good_channel.msg)
	
	@db_session
	def test_set_alert_channel(self):
		self.plugin.setAlertChannel(self.getGoodEvent())
		self.plugin.setAlertChannel(self.getGoodEventOtherChannel())
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		self.plugin.addToCollection(db.MonitoredMember, self.member2.id, self.getGoodEvent())
		
		msg = self.getContainMesssage()
		self.plugin.messageHandler(msg)
		
		self.assertEqual(len(self.member2.removed_roles), 1)
		self.assertEqual(self.member2.removed_roles[0], self.member.id)
		self.assertTrue(msg.deleteCalled)
		self.assertTrue("m2" in self.another_channel.msg)
		self.assertTrue("rehab" in self.another_channel.msg)
	
	@db_session
	def test_set_alert_channel_block(self):
		self.plugin.setAlertChannel(self.getBadUserEvent())
		self.plugin.addToCollection(db.MonitoredMember, self.member2.id, self.getGoodEvent())
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		msg = self.getContainMesssage()
		self.plugin.messageHandler(msg)
		
		self.assertEqual(len(self.member2.removed_roles), 1)
		self.assertEqual(self.member2.removed_roles[0], self.member.id)
		self.assertTrue(msg.deleteCalled)
		self.assertIsNone(self.good_channel.msg)
	
	def test_scan_roles1(self):
		ev = self.getGoodEvent()
		self.plugin.scanRoles(ev, "test")
		self.assertTrue("test1 - 1" in ev.rep)
		self.assertTrue("test2 - 2" in ev.rep)
		self.assertTrue("\n" in ev.rep)
	
	def test_scan_roles2(self):
		ev = self.getGoodEvent()
		self.plugin.scanRoles(ev, "test2")
		self.assertTrue("test1 - 1" not in ev.rep)
		self.assertTrue("test2 - 2" in ev.rep)

	def test_scan_roles_block(self):
		ev = self.getBadUserEvent()
		self.plugin.scanRoles(ev, "test")
		self.assertIsNone(ev.rep)
	
	@db_session
	def test_get_roles_list(self):
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		self.plugin.addToCollection(db.RehabRole, self.rehabRole.id, self.getGoodEvent())
		self.plugin.addToCollection(db.RehabRole, 100, self.getGoodEvent())
		
		ev = self.getGoodEvent()
		self.plugin.listRoles(ev)
		
		self.assertTrue("**Member roles**" in ev.rep)
		self.assertTrue("member test2 - 2" in ev.rep)
		self.assertTrue("Role not present - 100" in ev.rep)
	
	@db_session
	def test_get_roles_list_block(self):
		self.plugin.addToCollection(db.MemberRole, self.member.id, self.getGoodEvent())
		self.plugin.addToCollection(db.RehabRole, self.rehabRole.id, self.getGoodEvent())
		self.plugin.addToCollection(db.RehabRole, 100, self.getGoodEvent())
		
		ev = self.getBadUserEvent()
		self.plugin.listRoles(ev)
		
		self.assertIsNone(ev.rep)
		
		
		
		
		
		
if __name__ == '__main__':
	unittest.main()
