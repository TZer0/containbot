from datetime import datetime
from disco.bot import Plugin
from disco.bot import parser
from disco.types.message import MessageEmbed
from db import *
from disco.types.base import Unset
import disco.util.snowflake as snowflake

class ContainBot(Plugin):
	def __init__(self, bot, config):
		Plugin.__init__(self, bot=bot, config=config)
		if not self.client.config.configured_db:
			setupDb(self.client.config.db)
		self.badWords = ["kill", "murder"]
	
	def getExistingOrEmpty(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return collection(id=id, guild_id=guild_id)
			
	def getObjectOrNone(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return None
	
	def getSetting(self, collection, guild_id):
		if collection.exists(guild_id=guild_id):
			return collection.get(guild_id=guild_id)
		else:
			return None
			
	
	def checkAuthor(self, id):
		return id in self.client.config.admins		
	
	def addToCollection(self, collection, id, event):
		if not self.checkAuthor(event.author.id):
			return
		self.getExistingOrEmpty(collection, id, event.guild.id)
		commit()
	
	def removeFromCollection(self, collection, id, event):
		if not self.checkAuthor(event.author.id):
			return
		
		badRole = self.getObjectOrNone(collection, id, event.guild.id)
		if (badRole != None):
			badRole.delete()
			commit()
			return True
		return False
	
	def getRoleNamed(self, id, event):
		if id in event.guild.roles:
			return "%s - %d" % (event.guild.roles[id].name, id)
		return "Role not present - %d" % (id, )
	
	@Plugin.command('scanroles', '<name:str>')
	def scanRoles(self, event, name):
		if not self.checkAuthor(event.author.id):
			return
		ret = ""
		for id in event.guild.roles:
			if name not in event.guild.roles[id].name or event.guild.roles[id].name == "@everyone":
				continue
			if len(ret) != 0:
				ret += "\n"
			ret += "%s - %d" % (str(event.guild.roles[id].name), id)
		event.msg.reply(ret)

	@db_session
	@Plugin.command('listroles')
	def listRoles(self, event):
		if not self.checkAuthor(event.author.id):
			return
		guild_id = event.guild.id
		ret = "**Member roles**\n"
		for member in select(r for r in MemberRole if r.guild_id == guild_id)[:]:
			ret += self.getRoleNamed(member.id, event) + "\n"
		ret += "\n**Rehab roles**\n"
		for rehab in select(r for r in RehabRole if r.guild_id == guild_id)[:]:
			ret += self.getRoleNamed(rehab.id, event) + "\n"
		event.msg.reply(ret)

	@db_session
	@Plugin.command('setalertchannel')
	def setAlertChannel(self, event):
		if not self.checkAuthor(event.author.id):
			return
		guild_id = event.guild.id
		if AlertChannel.exists(guild_id=guild_id):
			AlertChannel.get(guild_id=guild_id).delete()
			commit()
		AlertChannel(guild_id=guild_id, channel_id=event.channel.id)
		commit()
		event.msg.reply('Done')
		
	@db_session
	@Plugin.command('addmemberrole', '<id:int>')
	def addMemberRole(self, event, id):
		self.addToCollection(MemberRole, id, event)
		event.msg.reply('Done')
	
	@db_session
	@Plugin.command('removememberrole', '<id:int>')
	def removeMemberRole(self, event, id):
		self.removeFromCollection(MemberRole, id, event)
		event.msg.reply('Done')
		
	@db_session
	@Plugin.command('addrehabrole', '<id:int>')
	def addRehabRole(self, event, id):
		self.addToCollection(RehabRole, id, event)
		event.msg.reply('Done')
	
	@db_session
	@Plugin.command('removerehabrole', '<id:int>')
	def removeRehabRole(self, event, id):
		self.removeFromCollection(RehabRole, id, event)
		event.msg.reply('Done')
	
	@db_session
	@Plugin.command('addmember', '<id:int>')
	def addMember(self, event, id):
		self.addToCollection(MonitoredMember, id, event)
		event.msg.reply('Done')
	
	@db_session
	@Plugin.command('removemember', '<id:int>')
	def removeMember(self, event, id):
		self.removeFromCollection(MonitoredMember, id, event)
		event.msg.reply('Done')
		
	@db_session
	@Plugin.listen('MessageCreate')
	def messageHandler(self, message):
		cLower = message.content.lower()
		found = False
		guild_id = message.guild.id
		for w in self.badWords:
			if w in cLower:
				found = True
				break
		
		if found:
			if self.getObjectOrNone(MonitoredMember, message.member.id, guild_id) != None:
				message.delete()
				for toRemove in select(r for r in MemberRole if r.guild_id == guild_id)[:]:
					message.member.remove_role(toRemove.id)
				for toAdd in select(r for r in RehabRole if r.guild_id == guild_id)[:]:
					message.member.add_role(toAdd.id)
				alertChannel = self.getSetting(AlertChannel, guild_id)
				if alertChannel != None:
					message.guild.channels[alertChannel.channel_id].send_message("%s(<@%d>) just got sent to containment, message: %s" % (str(message.member), message.member.id, message.content))
		
		
