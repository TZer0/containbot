FROM ubuntu:17.10
MAINTAINER TZer0

RUN apt-get update
RUN apt-get install -y python3.6 python3-pip
RUN pip3 install disco-py ujson pony
RUN mkdir /contain
ADD *.py /contain/

CMD cd /contain && python3 -m disco.cli --config config.json
